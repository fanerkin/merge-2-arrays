var
	merge = require('../'),
	test = require('tape');

test('normal', function (t) {
	t.plan(1);
	t.deepEqual(
		merge(
			[
				{ 'id': 2, 'value': 'someval', 'score': 2 },
				{ 'id': 1, 'value': 'someval', 'score': 1 },
				{ 'id': 4, 'value': 'someval', 'score': 1 }
			],
			[
				{ 'id': 3, 'value': 'someval', 'score': 1 },
				{ 'id': 2, 'value': 'someval', 'score': 1 }
			]
		),
		[
			{ 'id': 1, 'value': 'someval', 'score': 1 },
			{ 'id': 2, 'value': 'someval', 'score': 2 },
			{ 'id': 3, 'value': 'someval', 'score': 1 },
			{ 'id': 4, 'value': 'someval', 'score': 1 }
		]);
});

test('equal ids & equal scores', function (t) {
	t.plan(1);
	t.throws(
		function () {
			merge(
				[{ 'id': 1, 'value': 'someval', 'score': 1 }],
				[{ 'id': 1, 'value': 'someval', 'score': 1 }]
			);
		},
		/equal ids & equal scores/
	);
});
