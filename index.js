function sorter(a, b) { return a.id - b.id; }

function merge(elements, candidates) {
	var candidate,
		element,
		result = [];

	elements = elements.slice().sort(sorter);
	candidates = candidates.slice().sort(sorter);

	element = elements.shift();
	candidate = candidates.shift();

	while (element && candidate) {
		if ( candidate.id == element.id ) {
			if (candidate.score == element.score)
				throw new Error('equal ids & equal scores');
			if (candidate.score > element.score) result.push(candidate);
			else result.push(element);
			element = elements.shift();
			candidate = candidates.shift();
		} else if (candidate.id < element.id) {
			result.push(candidate);
			candidate = candidates.shift();
		} else {
			result.push(element);
			element = elements.shift();
		}
	}

	if (element) {
		result.push(element);
		result = result.concat(elements);
	} else if (candidate) {
		result.push(candidate);
		result = result.concat(candidates);
	}

	return result;
}

module.exports = merge;
