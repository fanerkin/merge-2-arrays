# a solution for [this question](http://stackoverflow.com/questions/19350802/javascript-synchronize-2-arrays-of-objects)

* usage: require('path-to-index.js/')
* test: npm test
* merges in O(n*log(n)) because of sorting

## quote from the task

For example, I have two arrays of objects
```javascript
[
 {
   'id':1,
   'value':'someval for id 1',
   'score':1
 },
 {
   'id':2,
   'value':'someval for id 2 with score 2',
   'score':2
 },
 {
   'id':4,
   'value':'someval for id 2',
   'score':1
 }
]
```
and
```javascript
[
 {
   'id':2,
   'value':'someval for id 2 ',
   'score':1
 },
 {
   'id':3,
   'value':'someval for id 3',
   'score':1
 }
]
```
I want to synchronize both of them to be like this
```javascript
[
 {
   'id':1,
   'value':'someval for id 1',
   'score':1
 },
 {
   'id':2,
   'value':'someval for id 2 with score 2',
   'score':2
 },
 {
   'id':3,
   'value':'someval for id 3',
   'score':1
 },
 {
   'id':4,
   'value':'someval for id 2',
   'score':1
 }
]
```
so, i want the sync to work on this rules

    if item with id is not present in one of arrays, it is copied from array where is is present
    if two items have the same id, we keep the one with higher score

Is there any libraries or node modules, that can do this synchronization for quite big arrays (nodejs modules preferred)?

Where shall i start my research to make this function in proper way?
